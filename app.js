const Koa = require('koa');
const Router = require('koa-router');
const KoaBody = require('koa-body');
const serve = require('koa-static');
const MobileDetect = require('mobile-detect');
const koaConvert = require('koa-convert')
const fs = require('fs');
const path = require('path');
const url = require('url');

const port = 3333;
const app = new Koa()

/**
 * 有了koa-body 就可以很方便的判断请求中的参数
 * ctx.request.query 和 ctx.request.body;
 */

app.use(KoaBody({
  multipart: true,
  formidable: {
    keepExtensions: true,
    hash: 'md5',
  },
}));

// 设置静态资源目录
app.use(koaConvert(serve(path.join(__dirname, './public'), {
  maxage: 1000 * 60 * 60 * 24,
})));

app.use(async (ctx, next) => {
  const md = new MobileDetect(ctx.headers['user-agent']);
  ctx.isMobile = (md.mobile() != null);

  await next()
})

const default_router = new Router();

default_router.all('/a', async (ctx) => {
  if (ctx.isMobile) {
    ctx.body = 'mobile'
  } else {
    ctx.body = 'pc'
  }
});

default_router.all('/b', async (ctx) => {
  const stream = fs.createReadStream('./public/hello.txt')
  console.log('stream', stream)
  ctx.body = stream
})

default_router.all('/c', async (ctx) => {
  const reqParams = ctx.method === 'GET' ? ctx.request.query : ctx.request.body;

  ctx.body = reqParams
})

app.use(default_router.routes());

// 启动服务
app.listen(port, '0.0.0.0', (err) => {
  if (!err) {
    console.log(`app start on ${port}`)
  }
});
