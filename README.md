## luffy

#### 进入目录
```
cd node_luffy
```

####  安装依赖
```
npm install
```

#### 启动
```
node app.js
```

#### 浏览器查看
```
http://localhost:3333/hello.txt

http://localhost:3333/luffy.jpeg

http://localhost:3333/a

http://localhost:3333/b

http://localhost:3333/c?hello=luffy
```
